//FUNCIONES

// const saludar = () => console.log("Hola");
// const saludarUsuario = (user) => console.log(`Hola ${user}`);


// saludarUsuario('Gracian');

// const suma = (num1, num2) =>{
//     if(num1 == 8){
//         return num1+num2
//     }
//     return num1
    
// }

// console.log(suma(8,8));

// const suma = (num1, num2) => num1+num2

// let restult = suma(3,6)

// console.log(restult)

//CLASES 

// class Persona {
//     constructor(nombre, apellido, edad){
//         this.nombre = nombre
//         this.apellido = apellido
//         this.edad = edad
//         this.datos = `Me llamo ${nombre} ${apellido} y tengo ${edad} años`
//     }
//     saludar(){
//         return `Hola, me llamo ${this.nombre} y tengo ${this.edad} años.`
//     }
// }

// const juan = new Persona('Juan', 'Garcia', '28')
// const marta = new Persona('Marta', 'Garcia', '45')

// console.log(marta.saludar());
// console.log(marta);

//PRACTICA

// class Libro {
//     constructor(titulo, autor, año, genero){
//         this.titulo = titulo
//         this.autor = autor
//         this.año = año
//         this.genero = genero
//     }
//     contenido(){
//         return `El siguiente libro se llama ${this.titulo} y su autor fue el gran ${this.autor} que creo este libro en el año de ${this.año} y perteneciente al genero ${this.genero}`
//     }

//     getAuthor(){
//         return this.autor
//     }

//     getGender(){
//         return this.genero
//     }
// }

// let libro =[]

// while(libro.length < 3 ){
//     let titulo = prompt('Introduce el titulo del libro')
//     let autor = prompt('Introduce el autor del libro')
//     let año = prompt('Introduce el año del libro')
//     let genero = prompt('Introduce el genero del libro').toLowerCase();

//     if(titulo != '' && autor != '' && !isNaN(año) && año.length == 4 && (genero == 'aventura' || genero == 'terror' || genero == 'fantasia')){
//         libro.push(new Libro(titulo, autor, año, genero))
//     }
// }

// const showAllBooks = () => {
//     console.log(libro);
// }

// const showAuthors = () => {
//     let autor = []
//     for(const libr of libro){
//         autor.push(libr.getAuthor());
//     }
//     console.log(autor.sort());
// }

// const showGender = () => {
//     const genero = prompt('Introduce el genero a buscar')
//     for(const libr of libro){
//         if(libr.getGender() == genero){
//             console.log(libr.showGender())
//         }
//     }
// }

// showGender();

// // Arrays - Métodos 
// //.from(iterable) - Convierte en array el elemento iterable

// let word = 'Hola mundo'

// console.log(Array.from(word));

//.sort([callback]) - Ordena los elementos de un array alfabéticamente(valor Unicode), si le pasamos un callback los ordena
//                    en función del algoritmo que le pasemos.

// const letters = ['b','c', 'z', 'a']
// const numbers = [1,8,100,300,3]

// console.log(numbers.sort((a,b)=> b-a));


// .forEach(callback(currentValue, [index])) - ejecuta la función indicada una vez por cada elemento del array.

// const numbers = [12,25,47,85,98]
// numbers.forEach((number, index) => console.log(`${number} esta en la posición ${index}`));

// .some (callback) - Comprueba si al menos un elemento del array cumple la condición

//. every (callback) - Comproueba si todos los elementos del array cumplen la condición

// const words = ['HTML', 'CSS', 'JavaScript', 'PHP']

// console.log(words.some(word => word.length>9));
// console.log(words.every(word => word.length>5));

//.map(callback) - Transforma todos los elementos del array y devuelve un nuevo array

// const numbers = [12,25,47,85,98]

// const numbers2 = numbers.map((number) => number * 2)

// console.log(numbers2);


// .filter(callback) - Filtra todos los elementos del array que cumplan la condición y devuelve un nuevo array

// const numbers = [12,25,47,85,98]

// const numbers2 = numbers.filter(number => number > 80)

// console.log(numbers2);

//SPREAD OPERATOR

const numbers = [-12, 2, 3, 23, 43, 2, 3]

// console.log(...numbers);

//ENVIAR ELEMENTOS DE UN ARRAY A UNA FUNCION

// const addNumbers = (a, b, c) => {
//     console.log(a+b+c);
// }
// let numersToAdd = [1,2,3]

// addNumbers(...numersToAdd)

//AÑADIR UN ARRAY A OTRO ARRAY

// let users = ['javier', 'david', 'rosa', 'juan', 'mercedes']
// let newUsers = ['marta', 'jaime', 'laura']

// users.push(...newUsers);

// console.log(users)


//Copiar Arrays

// let arr1 = [1, 2, 3, 4];
// let arr2 = [...arr1]
// console.log(arr2);

// CONCATENAR ARRAYS

// let arr1 = [1, 2, 3, 4];
// let arr2 = [6, 7 , 8]

// let arrConcat = arr1.concat(arr2)
// console.log(arrConcat);

// ENVIAR UN NÚMERO INDEFINIDO DE ARGUMENTOS A UNA FUNCION (parametros REST)

// const restParams = (...numbers) => {
//     console.log(numbers);
// }
// restParams();

//document.getElementById('id') - Acceder a un elemento a través de su id

// const title = document.getElementById('title')

// title.textContent = 'Dom - Accediendo a nodos'


//document.querySelector('selectorCSS') - Accede al primer elemento que coincida con el selector CSS

// const paragraph = document.querySelector('.paragraph')

// const span = paragraph.querySelector("span")

// console.log(span.textContent);

//docuemnt.querySelectorAll('selectorCSS') - Accede a todos los elementos que coincidan con el selector CSS, devuelve nodeList

// const paragraphs = document.querySelectorAll('.paragraph')
// const paragraphsSpread = [...document.querySelectorAll('.paragraph')]
// const paragraphsArray = Array.from(document.querySelectorAll('.paragraph'))

// paragraphs[0].style.color = 'red'

// // paragraphsSpread.map(p => p.style.color = 'green')

// paragraphsArray.map(p =>p.style.color='#592104')

// ATRIBUTOS
    //element.getAttribute('attribute')
    //element.setAttribute('attribute', value)

    //CLASES
        //element.classList.add('class', 'class', ...)
        //element.classList.remove('class','class',...)
        //element.classList.contains('class')
        //element.classList.replace('oldClass', newClass)
    
    //ATRIBUTOS DIRECTOS
        // ID
        // VALUE   
    
    const title = document.getElementById('title')
    const name = document.getElementById('name')

//console.log(name.setAttribute('type', 'radio'));        

 //title.classList.add('main-title', 'other-title')

  // title.classList.remove('main-title', 'other-title')

//   if(title.classList.contains('title')) console.log ('Title tiene la clase title')
//   else console.log( 'Title no tiene la clase title')

// console.log(title.innerHTML)

// const button = document.getElementById('button')
const box = document.getElementById('box')


// button.addEventListener('click', () =>{
//     console.log('CLICK');
// })

// button.addEventListener('dblclick', () =>{
//     console.log('DOBLE CLICK')
// })

// box.addEventListener('mouseenter', () =>{
//     box.classList.add('green')
// })

// box.addEventListener('mouseleave', () =>{
//     box.classList.remove('green')
//     box.classList.add('red')
// })

// box.addEventListener('mousedown', () =>{
//     console.log("Has pulsado en la caja");
// })

const form = document.getElementById('form')
const input = document.getElementById('input')
const button = document.getElementById('button')

// input.addEventListener('keyup', () =>{
//     console.log(event);
// })

button.addEventListener('click', (e) => {
    console.log(e.target);
})