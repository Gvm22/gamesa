$(document).ready(function(){
    $("#card-carousel").owlCarousel({
        autoPlay: true,
        items: 1,
        itemsDesktop : [992,1],
        itemsDesktopSmall : [768,2]
    })
})

$(document).ready(function(){
    $("#card-carousels").owlCarousel({
        autoPlay: 5000,
        items: 1,
        itemsDesktop : [992,2],
        itemsDesktopSmall : [768,2],
        
    })
})

$(document).on("click", 'a.come.mobile', function(e) {
    e.preventDefault();
    var i = $(this).find("i");
    var m = $(this).parents("footer").find(".menu-footer-mobile");
    if (m.length) {
        m.slideToggle("fast", function() {
            if (i.hasClass("fa-chevron-down"))
                i.removeClass("fa-chevron-down").addClass("fa-chevron-up");
            else
                i.removeClass("fa-chevron-up").addClass("fa-chevron-down");
            //verificamos si el footer este en un modal y calculamos en base a ello
            var modal_parent = $(i).parents('.modal'); 
            if(modal_parent.length == 0){
              $("html, body").animate({
                  scrollTop: $(document).height()
              });
            }else{
              $(modal_parent).animate({
                  scrollTop: $(modal_parent).height()
              });
            }
        });
    }
});